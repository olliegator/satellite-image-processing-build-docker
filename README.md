# Docker image for maven builds of satellite image processing tools

Contains:

- SeaDAS OCSSW processing package for MODIS Aqua
- Java JAI
- GDAL
- Maven

